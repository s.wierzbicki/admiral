<!DOCTYPE html>
<head>
    <script src="https://kit.fontawesome.com/a07ea887fb.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="public/css/style.css" />
    <title>Dashboards</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img class="logo" src="public/img/logo.svg" />
        <img class="user-icon" src="public/img/user.jpg" />
        <button class="nav_button"><i class="far fa-clipboard"></i></button>
        <button class="nav_button"><i class="fas fa-tools"></i></button>
        <button class="nav_button"><i class="fas fa-power-off"></i></button>
    </nav>
    <main>
        <div class="title_box"><p>DASHBOARDS</p></div>
        <section class="board">

        </section>
    </main>
</div>
</body>
