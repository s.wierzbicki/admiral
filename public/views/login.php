<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/login.css" />
    <title>Login Page</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg" /> 
        </div>
        <div class="login-container">
            <form action="login" method="POST">
                <div class="messages" >
                    <?php if(isset($messages)){
                        foreach($messages as $message){
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <label>LOGIN</label>
                <input name="login" type="text" placeholder="user" />
                <label>PASSWORD</label>
                <input name="password" type="password" placeholder="password" />
                </br>
                <button type="submit">LOGIN</button>
            </form>
        </div>
    </div>
</body>