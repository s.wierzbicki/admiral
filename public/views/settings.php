<!DOCTYPE html>
<head>
    <script src="https://kit.fontawesome.com/a07ea887fb.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="public/css/style.css" />
    <link rel="stylesheet" type="text/css" href="public/css/settings.css" />
    <script type="text/javascript" src="./public/js/image.js" defer></script>
    <title>Dashboards</title>
</head>
<body>
<div class="base-container">
    <?php include("navigation.php"); ?>
    <main>
        <div class="title_box"><p>Administrator Panel</p></div>
        <section class="board">
            <div class="settings">
                <div class="settingbox">
                    <a href="#" class="setting_div">
                        <button class="setting_button"><i class="fas fa-user"></i></button>
                        <h3>USERS</h3>
                    </a>
                    <a href="#" class="setting_div">
                        <button class="setting_button"><i class="fas fa-users"></i></button>
                        <h3>GROUPS</h3>
                    </a>
                    <a href="/invite" class="setting_div">
                        <button class="setting_button"><i class="fas fa-envelope"></i></button>
                        <h3>INVITE</h3>
                    </a>
                </div>
                <div class="settingbox">
                    <a href="#" class="setting_div">
                        <button class="setting_button"><i class="fas fa-book"></i></button>
                        <h3>JOURNAL</h3>
                    </a>
                    <a href="#" class="setting_div">
                        <button class="setting_button"><i class="fas fa-pallet"></i></button>
                        <h3>COMPONENTS</h3>
                    </a>
                    <a href="#" class="setting_div">
                        <button class="setting_button"><i class="fas fa-wrench"></i></button>
                        <h3>SETTINGS</h3>
                    </a>
                </div>
                <div class="user_box">
                    <div class="profile_picture">
                        <img class="user-icon" id="user-picture2" src="public/uploads/<?= $_SESSION['login'].".jpeg"; ?>" onerror="swapimage()"/>
                        <form action="upload" method="POST" enctype="multipart/form-data">
                            <?php if(isset($messages)){
                                foreach($messages as $message){
                                    echo $message;
                                }
                            }
                            ?>
                            <input type="file" name="filename" onchange="javascript:this.form.submit();">
                        </form>
                    </div>
                    <h2>Administrator</h2>
                    <h2>administrator@admiral.pl</h2>
                    <a href="#" class="setting_button">change email</a>
                    <a href="#" class="setting_button">change password</a>
                </div>
            </div>
        </section>
    </main>
</div>
</body>
