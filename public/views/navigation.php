<nav>
    <img class="logo" src="public/img/logo.svg" />
    <img class="user-icon" id="user-picture" src="public/uploads/<?= $_SESSION['login'].".jpeg"; ?>" onerror="swapimage()"/>
    <a href="/board" class="nav_button"><i class="far fa-clipboard"></i></a>
    <a href="/settings" class="nav_button"><i class="fas fa-tools"></i></a>
    <a href="/logout" class="nav_button"><i class="fas fa-power-off"></i></a>
</nav>