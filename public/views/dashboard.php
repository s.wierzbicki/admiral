<!DOCTYPE html>
<head>
    <script src="https://kit.fontawesome.com/a07ea887fb.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="public/css/style.css" />
    <link rel="stylesheet" type="text/css" href="public/css/dashboard.css" />
    <script type="text/javascript" src="./public/js/image.js" defer></script>
    <title>Dashboards</title>
</head>
<body>
    <div class="base-container">
        <?php include("navigation.php"); ?>
        <main>
            <div class="title_box"><p>DASHBOARDS</p></div>
            <section class="board">
                <div class="navigator">
                    <button class="navigator_button"><i class="fas fa-arrow-left"></i></button>
                    <button class="navigator_button"><i class="fas fa-plus-circle"></i></button>
                    <div class="dashboard-names">
                        <button class="dashboard_button">aaaaaaa</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DAssARD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DASHBssssssOARD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DsssssD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                        <button class="dashboard_button">DASHBOARD</button>
                    </div>
                    <button class="navigator_button"><i class="fas fa-arrow-right"></i></button>
                </div>
                <div class="pods">
                    <div class="pod" id="pod1">
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-pause"></i></button>
                            <div class="led"><i id="pod1cir" class="fas fa-circle"></i></div>
                        </div>
                        <div class="text_box">
                            <div class="text">
                                <p>BANK1 Tomcat</p>
                                <p>http://bank1.firma.com.pl</p>
                            </div>
                            <div class="text">
                                <p>User: Jan Kowalski</p>
                                <p>Branch: 03.02-Dev</p>
                            </div>
                            <div class="text">
                                <p>Wersja Bazy: 3.2.123.442</p>
                                <p>Uptime: 2h 30m</p>
                            </div>
                        </div>
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-folder-open"></i></button>
                            <button class="pod-button"><i class="fas fa-chart-area"></i></button>
                            <button class="pod-button"><i class="fab fa-gitlab"></i></button>
                        </div>
                    </div>
                    <div class="pod" id="pod2">
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-play"></i></button>
                            <div class="led"><i id="pod1cir" class="fas fa-circle"></i></div>
                        </div>
                        <div class="text_box">
                            <div class="text">
                                <p>BANK1 Tomcat</p>
                                <p>http://bank1.firma.com.pl</p>
                            </div>
                            <div class="text">
                                <p>User: Jan Kowalski</p>
                                <p>Branch: 03.02-Dev</p>
                            </div>
                            <div class="text">
                                <p>Wersja Bazy: 3.2.123.442</p>
                                <p>Downtime: 2d 1h 30m</p>
                            </div>
                        </div>
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-folder-open"></i></button>
                            <button class="pod-button"><i class="fas fa-chart-area"></i></button>
                            <button class="pod-button"><i class="fab fa-gitlab"></i></button>
                        </div>
                    </div>
                    <div class="pod" id="pod3">
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-pause"></i></button>
                            <div class="led"><i id="pod1cir" class="fas fa-circle"></i></div>
                        </div>
                        <div class="text_box">
                            <div class="text">
                                <p>BANK1 Tomcat</p>
                                <p>http://bank1.firma.com.pl</p>
                            </div>
                            <div class="text">
                                <p>User: Jan Kowalski</p>
                                <p>Branch: 03.02-Dev</p>
                            </div>
                            <div class="text">
                                <p>Wersja Bazy: 3.2.123.442</p>
                                <p>Uptime: 2h 30m</p>
                            </div>
                        </div>
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-folder-open"></i></button>
                            <button class="pod-button"><i class="fas fa-chart-area"></i></button>
                            <button class="pod-button"><i class="fab fa-gitlab"></i></button>
                        </div>
                    </div>
                    <div class="pod" id="pod4">
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-play"></i></button>
                            <div class="led"><i id="pod1cir" class="fas fa-circle"></i></div>
                        </div>
                        <div class="text_box">
                            <div class="text">
                                <p>BANK1 Tomcat</p>
                                <p>http://bank1.firma.com.pl</p>
                            </div>
                            <div class="text">
                                <p>User: Jan Kowalski</p>
                                <p>Branch: 03.02-Dev</p>
                            </div>
                            <div class="text">
                                <p>Wersja Bazy: 3.2.123.442</p>
                                <p>Downtime: 2h 30m</p>
                            </div>
                        </div>
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-folder-open"></i></button>
                            <button class="pod-button"><i class="fas fa-chart-area"></i></button>
                            <button class="pod-button"><i class="fab fa-gitlab"></i></button>
                        </div>
                    </div>
                    <div class="pod" id="pod5">
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-pause"></i></button>
                            <div class="led"><i id="pod1cir" class="fas fa-circle"></i></div>
                        </div>
                        <div class="text_box">
                            <div class="text">
                                <p>BANK1 Tomcat</p>
                                <p>http://bank1.firma.com.pl</p>
                            </div>
                            <div class="text">
                                <p>User: Jan Kowalski</p>
                                <p>Branch: 03.02-Dev</p>
                            </div>
                            <div class="text">
                                <p>Wersja Bazy: 3.2.123.442</p>
                                <p>Uptime: 2h 30m</p>
                            </div>
                        </div>
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-folder-open"></i></button>
                            <button class="pod-button"><i class="fas fa-chart-area"></i></button>
                            <button class="pod-button"><i class="fab fa-gitlab"></i></button>
                        </div>
                    </div>
                    <div class="pod" id="pod6">
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-pause"></i></button>
                            <div class="led"><i id="pod1cir" class="fas fa-circle"></i></div>
                        </div>
                        <div class="text_box">
                            <div class="text">
                                <p>BANK1 Tomcat</p>
                                <p>http://bank1.firma.com.pl</p>
                            </div>
                            <div class="text">
                                <p>User: Jan Kowalski</p>
                                <p>Branch: 03.02-Dev</p>
                            </div>
                            <div class="text">
                                <p>Wersja Bazy: 3.2.123.442</p>
                                <p>Uptime: 2h 30m</p>
                            </div>
                        </div>
                        <div class="pod-buttons">
                            <button class="pod-button"><i class="fas fa-folder-open"></i></button>
                            <button class="pod-button"><i class="fas fa-chart-area"></i></button>
                            <button class="pod-button"><i class="fab fa-gitlab"></i></button>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
</body>