<!DOCTYPE html>
<head>
    <script src="https://kit.fontawesome.com/a07ea887fb.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="public/css/style.css" />
    <link rel="stylesheet" type="text/css" href="public/css/invite.css" />
    <script type="text/javascript" src="./public/js/script.js" defer></script>
    <script type="text/javascript" src="./public/js/image.js" defer></script>
    <title>Dashboards</title>
</head>
<body>
<div class="base-container">
    <?php include("navigation.php"); ?>
    <main>
        <div class="title_box"><p>Invite user</p></div>
        <section class="board">
            <div class="settings">
                <div class="login-container">
                    <form class="register" action="register" method="POST">
                        <div class="messages">
                            <?php
                            if(isset($messages)){
                                foreach($messages as $message) {
                                    echo $message;
                                }
                            }
                            ?>
                        </div>
                        <input name="login" type="text" placeholder="login">
                        <input name="email" type="text" placeholder="email@email.com">
                        <input name="password" type="password" placeholder="password">
                        <input name="confirmedPassword" type="password" placeholder="confirm password">
                        <input name="name" type="text" placeholder="name">
                        <input name="surname" type="text" placeholder="surname">
                        <button name="active" type="submit">REGISTER</button>
                    </form>
                </div>
            </div>
        </section>
    </main>
</div>
</body>
