<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('board', 'DefaultController');
Router::get('settings', 'DefaultController');
Router::get('logout','SecurityController');
Router::get('invite','DefaultController');
Router::post('login', 'SecurityController');
Router::post('upload', 'UserController');
Router::post('register', 'UserController');

Router::run($path);