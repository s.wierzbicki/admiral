--create roles tables
create table roles
(
    id serial not null,
    name varchar(100) not null,
    capabilities text
);

create unique index roles_id_uindex
    on roles (id);

alter table roles
    add constraint roles_pk
        primary key (id);

--role bindings table
create table role_bindings
(
    user_id int not null,
    role_id int not null
);

alter table role_bindings
    add constraint role_bindings_users_id_fk
        foreign key (user_id) references users
            on update cascade on delete cascade;

alter table role_bindings
    add constraint role_bindings_roles_id_fk
        foreign key (role_id) references roles
            on update cascade on delete cascade;

-- Add basic roles
INSERT INTO roles (name, capabilities) VALUES ('user',''),('administrator','FULL_CAPS');