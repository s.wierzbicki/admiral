alter table users
    add enabled bool default true not null;

alter table users
    add salt varchar(16);

alter table users alter column salt set not null;

create unique index users_id_user_details_uindex
    on users (id_user_details);

alter table users
    add constraint users_user_details_id_fk
        foreign key (id_user_details) references user_details
            on update cascade on delete cascade;

ALTER SEQUENCE users_id_seq RESTART WITH 1;
ALTER SEQUENCE user_details_id_seq RESTART WITH 1;

WITH identity AS (
    INSERT INTO user_details (name, surname) VALUES ('System','Account') RETURNING id
) INSERT INTO users (login, email, password, salt, enabled, id_user_details) VALUES (
                                                                                  'system',
                                                                                  'system@admiral.com',
                                                                                  'md55e13123d7ac604b20e507f0d8651df75',
                                                                                  'aaabbbcccdddeeef',
                                                                                  false,
                                                                                  (SELECT id from identity)
                                                                              );


WITH identity AS (
    INSERT INTO user_details (name, surname) VALUES ('admiral','admin') RETURNING id
) INSERT INTO users (login, email, password, enabled, id_user_details) VALUES (
                                                                                  'admin',
                                                                                  'admin@admiral.com',
                                                                                  'md5fbf65f0ee1cefca87d627b620f19a037',
                                                                                  'fffgggkkkhhhddda',
                                                                                  true,
                                                                                  (SELECT id from identity)
                                                                              );

INSERT INTO role_bindings (user_id, role_id) VALUES ((SELECT id FROM users WHERE login = 'admin'),2);
INSERT INTO role_bindings (user_id, role_id) VALUES ((SELECT id FROM users WHERE login = 'system'),2);

INSERT INTO groups (name) VALUES ('all'),('administrators');

INSERT INTO group_members (user_id, group_id) VALUES ((SELECT id FROM users WHERE login = 'admin'),1), ((SELECT id FROM users WHERE login = 'admin'),2);
INSERT INTO group_members (user_id, group_id) VALUES ((SELECT id FROM users WHERE login = 'system'),1), ((SELECT id FROM users WHERE login = 'system'),2);

