-- Skrypt tworzy tabele przechowującą użytkowników.

create table users
(
    id serial not null,
    login varchar(100) not null,
    name varchar(100) not null,
    surname varchar(100) not null,
    email varchar(255) not null,
    password varchar(255) not null
);

create unique index users_id_uindex
	on users (id);

create unique index users_email_uindex
	on users (email);

create unique index users_login_uindex
	on users (login);


alter table users
    add constraint users_pk
        primary key (id);