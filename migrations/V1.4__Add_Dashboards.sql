create table dashboards
(
    id serial not null,
    name varchar(255) not null,
    user_id int not null,
    query text not null
);

create unique index dashboards_id_uindex
    on dashboards (id);

alter table dashboards
    add constraint dashboards_pk
        primary key (id);

alter table dashboards
    add constraint dashboards_users_id_fk
        foreign key (user_id) references users;

-- Table for pods
create table pods
(
    id serial not null,
    name varchar not null,
    namespace varchar(100) not null,
    last_connect date,
    status bool not null
);

create unique index pods_id_uindex
    on pods (id);

alter table pods
    add constraint pods_pk
        primary key (id);

-- Connect pods to dashboard

create table dashboard_items
(
    dashboard_id int not null,
    pod_id int not null
);

alter table dashboard_items
    add constraint dashboard_items_dashboards_id_fk
        foreign key (dashboard_id) references dashboards
            on update cascade on delete cascade;



-- Connect dashboards to groups
create table dashboard_groups
(
    dashboard_id int not null,
    group_id int not null
);

alter table dashboard_groups
    add constraint dashboard_groups_dashboards_id_fk
        foreign key (dashboard_id) references dashboards
            on update cascade on delete cascade;

alter table dashboard_groups
    add constraint dashboard_groups_groups_id_fk
        foreign key (group_id) references groups
            on update cascade on delete cascade;
