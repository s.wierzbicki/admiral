--create logs datatable
create table logs
(
    id serial not null,
    user_id int not null,
    date date not null,
    level varchar(8) not null,
    value text not null
);

create unique index logs_id_uindex
    on logs (id);

alter table logs
    add constraint logs_pk
        primary key (id);

alter table logs
    add constraint logs_users_id_fk
        foreign key (user_id) references users
            on update cascade on delete cascade;

