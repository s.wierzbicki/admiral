-- create user-details table

create table user_details
(
    id serial not null,
    name varchar(100) not null,
    surname varchar(100) not null,
    profile_picture bytea
);

create unique index user_details_id_uindex
    on user_details (id);

alter table user_details
    add constraint user_details_pk
        primary key (id);

-- Alter old users table
alter table users
    add id_user_details int default 0 not null;

UPDATE users set id_user_details = id WHERE users.id_user_details = 0;

-- Populate data from old table in new one
INSERT INTO user_details (id, name, surname)
SELECT id, name, surname FROM users;

alter table users drop column name;
alter table users drop column surname;