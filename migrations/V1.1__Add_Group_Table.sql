
--Create group tables
create table groups
(
    id serial not null,
    name varchar(255) not null
);

create unique index groups_id_uindex
    on groups (id);

create unique index groups_name_uindex
    on groups (name);

alter table groups
    add constraint groups_pk
        primary key (id);

--Create group_members table to save user groups
create table group_members
(
    user_id int not null,
    group_id int not null
);

--Assign FK from users and groups tables
alter table group_members
    add constraint group_members_users_id_fk
        foreign key (user_id) references users
            on update cascade on delete cascade;

alter table group_members
    add constraint group_members_groups_id_fk
        foreign key (group_id) references groups
            on update cascade on delete cascade;



