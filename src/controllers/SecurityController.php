<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController {
    public function login(){

        if($this->check_session()){
            return $this->render('dashboard');
        }

        $userRepository = new UserRepository();

        if(!$this->isPost()){
            return $this->render('login', ['messages' => ["This was not POST method"]]);
        }

        $username = $_POST["login"];
        $password = md5(md5($_POST["password"]));

        $user = $userRepository->getUser($username);

        if(!$user){
            return $this->render('login', ['messages' => ['User with this login does not exists!']]);
        }

        if($user->getPassword() !== $password){
            return $this->render('login', ['messages' => ['Wrong Password                                   '.$password.'                     '.$user->getPassword()]]);
        }

        session_start();
        $_SESSION['login']=$user->getLogin();
        $_SESSION['role']=$user->getRole();
        $this->systemlog("INFO","User ".$_SESSION['login']." has logged in");
        setcookie("sesja",session_id(), time() + (60*20), "/");
        return $this->render('dashboard');
    }

    public function logout(){
        $this->systemlog("INFO","User ".$_SESSION['login']." has loged out");
        $this->check_session();
        unset($_SESSION['']);
        session_unset();
        session_destroy();
        setcookie("sesja", "",time()-6000, "/");

        return $this->render('login', ['messages' => ["Logged out!"]]);
    }
}