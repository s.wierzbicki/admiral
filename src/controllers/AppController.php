<?php
require_once __DIR__.'/../repository/LogRepository.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/Log.php';


class AppController {
    private $request;

    public function __construct(){
        $this->request = $_SERVER['REQUEST_METHOD'];
    }

    protected function check_session(): bool{
        if(isset($_COOKIE['sesja'])) {
            if(!session_id()) {
                session_id($_COOKIE['sesja']);
                session_start();
                return true;
            }
            if(session_id() != $_COOKIE['sesja']){
                session_destroy();
                session_id($_COOKIE['sesja']);
                session_start();
            }
        }
        return false;
    }

    protected function isPost(): bool{
        return $this->request === 'POST';
    }


    protected function isGet(): bool{
        return $this->request === 'GET';
    }

    protected function render(string $template = null, array $variables = []){
        $templatePath = 'public/views/'. $template.'.php';
        $output = 'File not found';

        if(file_exists($templatePath)){
            extract($variables);

            ob_start();
            include $templatePath;
            $output = ob_get_clean();
        }
        print $output;
    }


    protected function log(string $level, string $text){
        $user = $_SESSION['login'];
        $user_repository = new UserRepository();
        $logging = new Log($user_repository->getUserId($user), $level, $text);
        $log_repository = new LogRepository();
        $log_repository->addLog($logging);
    }

    protected function systemlog(string $level, string $text){
        $user = $_SESSION['login'];
        $user_repository = new UserRepository();
        $logging = new Log(1, $level, $text);
        $log_repository = new LogRepository();
        $log_repository->addLog($logging);
    }
}