<?php

require_once 'AppController.php';

class DefaultController extends AppController {

    public function index(){
        if($this->check_session()){
            return $this->render('dashboard');
        }
        return $this->render('login');
    }


    public function board(){
        if(!$this->check_session()){
            return $this->render('login');
        }
        return $this->render('dashboard');
    }

    public function settings(){
        if(!$this->check_session()){
            return $this->render('login');
        }
        return $this->render('settings');
    }

    public function invite(){
        if(!$this->check_session()){
            return $this->render('login');
        }
        if($_SESSION['role']!='administrator'){
            return $this->render('settings');
        }
        return $this->render('invite');
    }
}