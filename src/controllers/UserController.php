<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../repository/RoleRepository.php';
require_once __DIR__.'/../repository/LogRepository.php';
require_once __DIR__.'/../models/Log.php';

class UserController extends AppController {
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES=['image/png','image/jpeg'];
    const UPLOAD_DIRECTORY='/../public/uploads';

    private $messages = [];

    public function upload(){
        if(!$this->check_session()){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
        }
        if($this->isPost() && is_uploaded_file($_FILES['filename']['tmp_name']) && $this->validate($_FILES['filename'])){
            move_uploaded_file(
                $_FILES['filename']['tmp_name'], dirname(__DIR__).self::UPLOAD_DIRECTORY.'/'.$_SESSION['login'].'.jpeg'
            );
            return $this->render('settings');
        }
    }

    private function validate(array $filename): bool{
        if($filename['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = "File is too large for destination file system.";
            return false;
        }
        if(!isset($filename['type']) && !in_array($filename['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = "File type is not supported";
            return false;
        }

        return true;
    }

    public function register(){
        if(!$this->check_session()){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
        }

        if($this->isPost()){
            if(!$_SESSION['role']=="administrator"){
                $this->messages[] = "You are not administrator, you can't invite others.";
                return $this->render('settings');
            }
            $login = $_POST['login'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $confpassword = $_POST['confirmedPassword'];
            $name = $_POST['name'];
            $surname = $_POST['surname'];

            if(!$login){
                $this->messages[] = "Login can not be empty";
                $this->invite();
            }

            if(!$email){
                $this->messages[] = "Email can not be empty";
                $this->invite();
            }

            if(!$password || $password != $confpassword){
                $this->messages[] = "Invalid Password";
                $this->invite();
            }

            if(!$name || !$surname){
                $this->messages[] = "Personal details not set!";
                $this->invite();
            }

            $user_repository = new UserRepository();
            $user = $user_repository->getUser($email);
            if($user){
                $this->messages[] = "Email already exists!";
                $this->invite();
            }

            $user = $user_repository->getUser($login);
            if($user){
                $this->messages[] = "Login already exists!";
                $this->invite();
            }
            $user = $user_repository->getUser($email);
            if($user){
                $this->messages[] = "email already exists!";
                $this->invite();
            }

            $user = new User($email,md5(md5($password)),$login,$name,$surname,"user","hasdahasdahasdax");
            $user_repository->addUser($user);
            $role_repository = new RoleRepository();
            $role_repository->addUserToRole($user_repository->getUserId($login),"user");
            $logging = new Log($user_repository->getUserId($_SESSION['login']),"INFO","User ".$login." with email ".$email." added by ".$_SESSION['login']);
            $log_repository = new LogRepository();
            $log_repository->addLog($logging);
            $this->messages[] = "Success!";
            $this->invite();
        }
    }

    public function invite(){
        return $this->render('invite', ['messages' => $this->messages]);
    }
}