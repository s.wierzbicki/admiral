<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Group.php';
require_once __DIR__.'/../models/AdmiralException.php';

class GroupRepository extends Repository {
    public function getDashboard(string $name): ?Group{
        $statement = $this->database->connect()->prepare('SELECT * FROM public.groups WHERE name = :name');
        $statement->bindParam(':name', $name, PDO::PARAM_INT);

        try {
            $statement->execute();
        } catch(PDOException $e) {
            echo "PDOException when executing sql in DashboardRepository : ".$e->getMessage();
        }

        $group = $statement->fetch(PDO::FETCH_ASSOC);
        try {
            $this->checkDashboard($group, $name);
        } catch (AdmiralException $e){
            //echo $e;
            return null;
        }

        return new Group(
            $group['name'],
        );
    }

    public function addGroup(Group $group): void{
        $statement = $this->database->connect()->prepare(
            'INSERT INTO groups (name) values (?)'
        );

        $statement->execute([
            $group->getName()
        ]);
    }



    public function checkGroup(Group $group, int $id){
        if ($group == false){
            throw new AdmiralException('Group with id '.$id." is not found in database!");
        }
        return true;
    }
}