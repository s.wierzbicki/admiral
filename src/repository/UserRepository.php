<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../models/Log.php';
require_once __DIR__.'/../models/AdmiralException.php';
require_once __DIR__.'/../repository/LogRepository.php';


class UserRepository extends Repository {
    public function getUser(string $username): ?User{
        if(filter_var($username,FILTER_VALIDATE_EMAIL)){
            $statement = $this->database->connect()->prepare(
                'SELECT * FROM public.user_info WHERE email = :username'
            );
        } else {
            $statement = $this->database->connect()->prepare(
                'SELECT * FROM public.user_info WHERE login = :username'
            );
        }

        $statement->bindParam(':username', $username, PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch(PDOException $e) {
            echo "PDOException when executing sql in UserRepository : ".$e->getMessage();
        }

        $user = $statement->fetch(PDO::FETCH_ASSOC);
        try {
            $this->checkUser($user,$username);
        } catch (AdmiralException $e){
            //echo $e;
            return null;
        }

        return new User(
            $user['email'],
            $user['password'],
            $user['login'],
            $user['name'],
            $user['surname'],
            $user['role'],
            $user['salt']
        );
    }

    public function addUser(User $user){
        $stmt = $this->database->connect()->prepare('
            WITH identity AS ( INSERT INTO user_details (name, surname) VALUES (?,?) RETURNING id )
            INSERT INTO users (login, email, password, salt, enabled, id_user_details) VALUES (?, ?, ?, ?, True, (SELECT id from identity))
        ');

        $stmt->execute([
            $user->getName(),
            $user->getSurname(),
            $user->getLogin(),
            $user->getEmail(),
            $user->getPassword(),
            $user->getSalt(),
        ]);
    }

    public function checkUser($user, string $username){
        if ($user == false){
            throw new AdmiralException('User '.$username." is not found in database!");
        }
        return true;
    }

    public function getUserId(string $username){
        if(filter_var($username,FILTER_VALIDATE_EMAIL)){
            $statement = $this->database->connect()->prepare(
                'SELECT id FROM public.user_info WHERE email = :username'
            );
        } else {
            $statement = $this->database->connect()->prepare(
                'SELECT id FROM public.user_info WHERE login = :username'
            );
        }
        $statement->bindParam(':username', $username, PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch(PDOException $e) {
            echo "PDOException when executing sql in UserRepository : ".$e->getMessage();
            $logging = new Log(1,"FATAL","Error when executing getUserById with String ".$username);
            $log_repository = new LogRepository();
            $log_repository->addLog($logging);
        }
        $id = $statement->fetch(PDO::FETCH_ASSOC);

        return $id['id'];
    }

}