<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Log.php';
require_once __DIR__.'/../models/AdmiralException.php';

class LogRepository extends Repository {
    public function getLog(int $id): ?Log{
        $statement = $this->database->connect()->prepare('SELECT * FROM public.logs WHERE id = :id');
        $statement->bindParam(':id', $id, PDO::PARAM_INT);

        try {
            $statement->execute();
        } catch(PDOException $e) {
            echo "PDOException when executing sql in UserRepository : ".$e->getMessage();
        }

        $log = $statement->fetch(PDO::FETCH_ASSOC);

        return new Log(
            $log['user_id'],
            $log['date'],
            $log['level'],
            $log['value']
        );
    }

    public function addLog(Log $log){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO logs (user_id, date, level, value) VALUES (?,?,?,?)
        ');
        $stmt->execute([
            $log->getUserId(),
            $log->getDate(),
            $log->getLevel(),
            $log->getValue()
        ]);
    }
}