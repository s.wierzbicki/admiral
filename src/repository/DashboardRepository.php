<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Dashboard.php';
require_once __DIR__.'/../models/AdmiralException.php';

class DashboardRepository extends Repository {
    public function getDashboard(int $id): ?Dashboard{
        $statement = $this->database->connect()->prepare('SELECT * FROM public.user_info WHERE id = :id');
        $statement->bindParam(':id', $id, PDO::PARAM_INT);

        try {
            $statement->execute();
        } catch(PDOException $e) {
            echo "PDOException when executing sql in DashboardRepository : ".$e->getMessage();
        }

        $dashboard = $statement->fetch(PDO::FETCH_ASSOC);
        try {
            $this->checkDashboard($dashboard, $id);
        } catch (AdmiralException $e){
            //echo $e;
            return null;
        }

        return new Dashboard(
            $dashboard['name'],
            $dashboard['user_id'],
            $dashboard['query']
        );
    }

    public function addDashboard(Dashboard $dashboard): void{
        $statement = $this->database->connect()->prepare(
            'INSERT INTO dashboards (name, user_id, query) values (?,?,?)'
        );

        $statement->execute([
            $dashboard->getName(),
            $dashboard->getUser(),
            $dashboard->getQuery()
        ]);
    }



    public function checkDashboard($dashboard, int $id){
        if ($dashboard == false){
            throw new AdmiralException('Dashboard with id '.$id." is not found in database!");
        }
        return true;
    }
}