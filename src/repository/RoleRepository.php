<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../models/AdmiralException.php';

class RoleRepository extends Repository {
    public function addUserToRole(int $user_id, string $role){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO role_bindings (user_id, role_id) VALUES (:user_id, (SELECT id FROM roles WHERE name = :role))
        ');

        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->bindParam(':role', $role, PDO::PARAM_STR);

        $stmt->execute();
    }

    public function addRole(string $name, string $caps){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO roles (name, capabilities) VALUES (?,?)
        ');
        $stmt->execute([
            $name,
            $caps
        ]);
    }
}