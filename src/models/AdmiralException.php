<?php
class AdmiralException extends Exception{
    protected $details;

    public function __construct($details) {
        $this->details = $details;
        parent::__construct();
    }

    public function __toString() {
        return 'Yarr! Admiral is yelling: ' . $this->details;
    }
}