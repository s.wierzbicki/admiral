<?php


class Log{
    private $user_id;
    private $date;
    private $level;
    private $value;

    public function __construct($user_id, $level, $value){
        $this->user_id = $user_id;
        $this->date = date("Y-m-d H:i:s");
        $this->level = $level;
        $this->value = $value;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel($level): void
    {
        $this->level = $level;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }
}

