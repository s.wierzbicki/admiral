<?php


class Dashboard{
    private $name;
    private $user_id;
    private $query;

    public function __construct(string $name, int $user_id, string $query){
        $this->name = $name;
        $this->user_id = $user_id;
        $this->query = $query;
    }

    public function getName(): string
    {
        return $this->name;
    }


    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }


    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }


    public function getQuery(): string
    {
        return $this->query;
    }

    public function setQuery(string $query): void
    {
        $this->query = $query;
    }

}